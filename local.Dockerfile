ARG PHP_VERSION
FROM registry.gitlab.com/mkjmdski/php-base/${PHP_VERSION}:latest
RUN apk add --no-cache $PHPIZE_DEPS \
    && pecl install xdebug \
    && docker-php-ext-enable xdebug
RUN apk add --no-cache shadow && \
    usermod -u 1000 www-data && \
    groupmod -g 1000 www-data && \
    apk del shadow

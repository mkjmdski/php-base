ARG PHP_VERSION
FROM php:${PHP_VERSION}
ENV SITE_DIR=/app/backend
RUN apk add --update --no-cache \
        mysql-client \
        nginx \
        supervisor \
        msmtp
RUN rm -rf /etc/nginx/conf.d/*
COPY conf /
WORKDIR ${SITE_DIR}
CMD ["supervisord", "--configuration=/etc/supervisor/conf.d/app.conf"]

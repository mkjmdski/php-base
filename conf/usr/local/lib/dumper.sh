connect_mysql() {
    mysql \
        --user="${MYSQL_USERNAME}" \
        --password="${MYSQL_PASS}" \
        --host="${MYSQL_HOSTNAME}" \
        --port="${MYSQL_PORT}" "$@"
}

list_tables() {
     connect_mysql "${MYSQL_DATABASE}" --execute="show tables;" | awk '{print $1}' | tail -n +2
}

dump() {
    mysqldump \
        --extended-insert=${INSERT-FALSE} \
        --user="${MYSQL_USERNAME}" \
        --password="${MYSQL_PASS}" \
        --host="${MYSQL_HOSTNAME}" \
        --port="${MYSQL_PORT}" "$@"
}

load_dump() {
    name="$1"
    echo "Loading dump ${name}"
    if [ -f "$name" ]; then
        connect_mysql "${MYSQL_DATABASE}" < "${name}"
        echo "Dump ${name} loaded to ${MYSQL_DATABASE}"
    else
        >&2 echo "No dump has been found at ${name}!"
        exit 1
    fi

}

drop_db() {
    if check_db_exist; then
        connect_mysql --execute="DROP DATABASE ${MYSQL_DATABASE};"
        echo "Database ${MYSQL_DATABASE} droped"
    fi
}

create_db() {
    if ! check_db_exist; then
        connect_mysql --execute="CREATE DATABASE ${MYSQL_DATABASE};"
        echo "Database ${MYSQL_DATABASE} created"
    fi
}

check_db_exist() {
    if connect_mysql --execute="SHOW DATABASES LIKE '${MYSQL_DATABASE}';" | grep -q "${MYSQL_DATABASE}"; then
        echo 'db exists'
        return 0
    else
        >&2 echo 'no db'
        return 1
    fi
}
